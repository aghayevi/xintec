/*************************************
designed to permanently running process
once started created control file, loads patterns once, then cycles and sleeps 10 seconds. In main cycle included retrieving list from AGGREGATION_USG_TBL and reading data from DETECTION_USG_TBL
parameter_ids, operator_ids hardcoded in this program. If changed in definition tables this module suppose to be re-compiled. Field PARAMETER_DEF_TBL.PARAMETER_ABBR can be removed from table
I can define parameter_ids using define keyword later on. However I havent done it here but it is easy to do
before running please set DETECTION_USG_TBL.DETECTION_STATUS=0 so the module can fetch them
Insert that are done to ALARMS_USG_TBL is very raw, it dumps calculated non-rounded percentage values normalized to integers. Datetime function for CREATE_DATETIME hasn't been yet implemented in this module
To stop permanent process remove detector.IND file from the folder program was started
*************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "FWsql.h"
#include "FWlog.h"

#define CONTROL_FILENAME "detector.IND"

#define SLEEP_SECONDS 10

#define ARRAY_SIZE 40000

#define NORMALIZER 100000

	struct P_CURSOR {
	int	PATTERN_ID[ARRAY_SIZE];
	int	OPERATOR_1[ARRAY_SIZE];
	int	REF_VALUE_1[ARRAY_SIZE];
        int     OPERATOR_2[ARRAY_SIZE]; 
	int     REF_VALUE_2[ARRAY_SIZE];
        int     OPERATOR_3[ARRAY_SIZE];
        int     REF_VALUE_3[ARRAY_SIZE];
        int     OPERATOR_4[ARRAY_SIZE]; 
	int     REF_VALUE_4[ARRAY_SIZE];
        int     OPERATOR_5[ARRAY_SIZE];
        int     REF_VALUE_5[ARRAY_SIZE];
	int     OPERATOR_6[ARRAY_SIZE];
	int     REF_VALUE_6[ARRAY_SIZE];
	int     OPERATOR_7[ARRAY_SIZE];
	int     REF_VALUE_7[ARRAY_SIZE];
	int     OPERATOR_8[ARRAY_SIZE];
	int     REF_VALUE_8[ARRAY_SIZE];
	int     OPERATOR_9[ARRAY_SIZE];
	int     REF_VALUE_9[ARRAY_SIZE];
	int     OPERATOR_10[ARRAY_SIZE];
	int     REF_VALUE_10[ARRAY_SIZE];
	int     OPERATOR_11[ARRAY_SIZE];
	int     REF_VALUE_11[ARRAY_SIZE];
	int     OPERATOR_12[ARRAY_SIZE];
	int     REF_VALUE_12[ARRAY_SIZE];
	int     OPERATOR_13[ARRAY_SIZE];
	int     REF_VALUE_13[ARRAY_SIZE];
	int     OPERATOR_14[ARRAY_SIZE];
	int     REF_VALUE_14[ARRAY_SIZE];
	};

	struct A_CURSOR {
	int	aggregation_id[ARRAY_SIZE];
	char	start_datetime[ARRAY_SIZE][15];
	};


        struct D_CURSOR {
        int DETECTION_ID;
        char IMSI[15];
        int INTRA_NET_USAGE_CALLS;
        int INTER_NET_USAGE_CALLS;
        int MOBILE_NET_USAGE_CALLS;
        int INTERNATIONAL_USAGE_CALLS;
        int NATIONAL_USAGE_CALLS;
        int IMEI_DISTINCT_COUNT;
        int CELL_DISTINCT_COUNT;
        int GPRS_USAGE_CLICKS;
        int SMS_MO_USAGE_CLICKS;
        int SMS_MT_USAGE_CLICKS;
        int MTC_USAGE_CALLS;
        int MIN_MOB_NET_COUNT;
	
        int WHITE_LIST_ENTRY;
        int DISTINCT_USAGE_CALLS;
        int MOC_TOTAL_CALLS;
        int SMS_TOTAL_CLICKS;
        int GPRS_TOTAL_CLICKS;
        };

int cmp();
int cmp_P();
int cmp_N();
int cmp_B();

/* My global definitions */	
int complex[14];

int main()
{
    int             rc = 0;
    char            db_name[] = "sim_gateway.db";
    char            my_sql[2048];
    char*           err_msg = 0;

    sqlite3*        db = 0;
    sqlite3_stmt*   pStmt = 0;

	/* My local definitions */

	int pat_cur, pat_new, pat_cnt;
	int agg_cur, agg_cnt;
	int dbg_cur;

	char ctrl_flname[13] = CONTROL_FILENAME;
	struct stat st_ctrl;
	
	struct P_CURSOR p_cursor;
	struct A_CURSOR a_cursor;
	struct D_CURSOR d_cursor;

	/* Create file or quit */	
    if(stat(ctrl_flname, &st_ctrl)>=0)
     {
         FW_log("i", "Usage error:\nIt is runnig another  process");
	return 1;
     }
     else
     {          
          if(creat(ctrl_flname,S_IREAD|S_IWRITE)<0)
          {
          FW_log("i", "Cannot create control file");
	return 1;
          }
          FW_log("i", "Created control file");
	  FW_log("i", "Begin of process");
     	}
	
    /* Open our DB again  */

    FW_log("i", "Opening DB %s", db_name);
    if (FW_sqlite_db_open(&db, db_name))
    {
        FW_log("e", "Error opening DB: %s", db_name);
        return 1;
    }

	/* Prepare pattern structures */
	
	memset(&p_cursor, 0, sizeof(p_cursor));
    	memset(&my_sql, 0, sizeof(my_sql)); 
	
    /* Pattern Routine Begin */

	sprintf(my_sql, "SELECT p1.pattern_id, p2.parameter_id, c1.priority, \n \
	o2.operator_id,ref_integer,r2.ref_type_code \n \
	FROM pattern_usg_tbl p1,chain_usg_tbl c1,parameter_def_tbl p2, \n \
	condition_usg_tbl c2,operator_def_tbl o2,ref_value_usg_tbl r2 \n \
	WHERE c1.pattern_id=p1.pattern_id AND c2.chain_id=c1.chain_id \n \
	AND p2.parameter_id = c2.parameter_id AND o2.operator_id=c2.operator_id AND r2.condition_id=c2.condition_id \n \
	order by c1.pattern_id, c1.priority");

    do
    {
        rc = sqlite3_prepare(db, my_sql, -1, &pStmt, 0);
        if(rc != SQLITE_OK) {
            FW_log("e", "SQL error: %s", sqlite3_errmsg(db));
            FW_log("e", "SQL stat : %s", my_sql);
            sqlite3_free(err_msg);
            return 1;
        }

        while( SQLITE_ROW==sqlite3_step(pStmt) )
        {
	pat_cnt = (int) sqlite3_column_int(pStmt, 0);
	p_cursor.PATTERN_ID[pat_cnt] = (int) sqlite3_column_int(pStmt, 0);

	switch ((int) sqlite3_column_int(pStmt, 1)) {
	case 1:	//INTRA_NET_USAGE
	p_cursor.OPERATOR_1[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);
	p_cursor.REF_VALUE_1[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
	break;
	case 2:	//INTER_NET_USAGE
	p_cursor.OPERATOR_2[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);
	p_cursor.REF_VALUE_2[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
	break;
	case 3:	//MOBILE_NET_USAGE
        p_cursor.OPERATOR_3[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);
        p_cursor.REF_VALUE_3[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
        break;
	case 4:	//INTERNATIONAL_USAGE
        p_cursor.OPERATOR_4[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);
        p_cursor.REF_VALUE_4[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
        break;
	case 5:	//NATIONAL_USAGE
        p_cursor.OPERATOR_5[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);
        p_cursor.REF_VALUE_5[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
        break;
	case 6:	//CALL_DIVERSITY_SPREAD
	p_cursor.OPERATOR_6[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);
	p_cursor.REF_VALUE_6[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
	break;
	case 7: //IMEI_DISTINCT_COUNT     
	p_cursor.OPERATOR_7[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);       
	p_cursor.REF_VALUE_7[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
	break;
	case 8: //CELL_DISTINCT_COUNT     
	p_cursor.OPERATOR_8[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);       
	p_cursor.REF_VALUE_8[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
	break;
	case 9: //GPRS_USAGE     
	p_cursor.OPERATOR_9[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);       
	p_cursor.REF_VALUE_9[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
	break;
	case 10: //SMS_MO_USAGE     
	p_cursor.OPERATOR_10[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);       
	p_cursor.REF_VALUE_10[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
	break;
	case 11: //SMS_MT_USAGE     
	p_cursor.OPERATOR_11[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);       
	p_cursor.REF_VALUE_11[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
	break;
	case 12: //MTC_USAGE    
	p_cursor.OPERATOR_12[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);       
	p_cursor.REF_VALUE_12[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
	break;
	case 13: //MIN_MOB_NET_COUNT
	p_cursor.OPERATOR_13[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);       
	p_cursor.REF_VALUE_13[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
	break;
	case 14: //WHITE_LIST_ENTRY
	p_cursor.OPERATOR_14[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);       
	p_cursor.REF_VALUE_14[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
	break;
	}

        }

	pat_new = pat_cnt;
        rc = sqlite3_finalize(pStmt);

    } while( rc==SQLITE_SCHEMA );

	/* Pattern Routine End */
	
    	/* Print loaded pattern buffers */
	for(dbg_cur = 1;dbg_cur<=pat_new;dbg_cur++) {
	FW_log("i"," pattern %d | %d %d | %d %d | %d %d | %d %d | %d %d | %d %d | %d %d | %d %d | %d %d | %d %d | %d %d | %d %d | %d %d | %d %d ", 
		p_cursor.PATTERN_ID[dbg_cur], 
		p_cursor.OPERATOR_1[dbg_cur], p_cursor.REF_VALUE_1[dbg_cur], 
		p_cursor.OPERATOR_2[dbg_cur], p_cursor.REF_VALUE_2[dbg_cur], 
		p_cursor.OPERATOR_3[dbg_cur], p_cursor.REF_VALUE_3[dbg_cur], 
		p_cursor.OPERATOR_4[dbg_cur], p_cursor.REF_VALUE_4[dbg_cur], 
		p_cursor.OPERATOR_5[dbg_cur], p_cursor.REF_VALUE_5[dbg_cur], 
		p_cursor.OPERATOR_6[dbg_cur], p_cursor.REF_VALUE_6[dbg_cur], 
		p_cursor.OPERATOR_7[dbg_cur], p_cursor.REF_VALUE_7[dbg_cur],
		p_cursor.OPERATOR_8[dbg_cur], p_cursor.REF_VALUE_8[dbg_cur],
		p_cursor.OPERATOR_9[dbg_cur], p_cursor.REF_VALUE_9[dbg_cur],
		p_cursor.OPERATOR_10[dbg_cur], p_cursor.REF_VALUE_10[dbg_cur],
		p_cursor.OPERATOR_11[dbg_cur], p_cursor.REF_VALUE_11[dbg_cur],
		p_cursor.OPERATOR_12[dbg_cur], p_cursor.REF_VALUE_12[dbg_cur],
		p_cursor.OPERATOR_13[dbg_cur], p_cursor.REF_VALUE_13[dbg_cur],
		p_cursor.OPERATOR_14[dbg_cur], p_cursor.REF_VALUE_14[dbg_cur]
		);
	}


	/* Start of Main Cycle */
	while(1>0)
	{
	FW_log("i","Cycle starts");

	/* Checking Control File for existence */
     if(stat(ctrl_flname,&st_ctrl)<0)
     {
        FW_log("i","Control File Not Found, quiting");
	FW_log("i", "Close DB");
	if(sqlite3_close(db))
	{
        FW_log("e", "SQL error closing Work DB: %s", sqlite3_errmsg(db));
        return 1;
	}

	FW_log("i", "End of process");
	FW_resetProg();
        return rc;
     }

	/* Prepare aggregation structures */

	memset(&a_cursor, 0, sizeof(a_cursor));
	memset(&my_sql, 0, sizeof(my_sql));
	agg_cnt = 0;

	/* Aggregation Table Routine Begin */

	sprintf(my_sql,"select aggregation_id, start_datetime from aggregation_usg_tbl");

	do
	{
        rc = sqlite3_prepare(db, my_sql, -1, &pStmt, 0);
        if(rc != SQLITE_OK) {
            FW_log("e", "SQL error: %s", sqlite3_errmsg(db));
            FW_log("e", "SQL stat : %s", my_sql);
            sqlite3_free(err_msg);
            return 1;
        }

        while( SQLITE_ROW==sqlite3_step(pStmt) )
        {
	a_cursor.aggregation_id[agg_cnt] = (int) sqlite3_column_int(pStmt, 0);
	//strncpy(a_cursor.start_datetime[agg_cnt], (constr char *) sqlite3_column_text(pStmt, 1), sizeof(a_cursor.start_datetime[agg_cnt]));
	agg_cnt++;
        }

        rc = sqlite3_finalize(pStmt);

    } while( rc==SQLITE_SCHEMA );

    	/* Aggregation Table Routine End */

	/* Detection Table Cycle Begin */
	
	for (agg_cur=0; agg_cur<agg_cnt; agg_cur++) {

	/* Prepare Detection Structures 1 */
        memset(&my_sql, 0, sizeof(my_sql));

	sprintf(my_sql,"select DETECTION_ID, IMSI, INTRA_NET_USAGE_CALLS, INTER_NET_USAGE_CALLS, MOBILE_NET_USAGE_CALLS, \n \
	INTERNATIONAL_USAGE_CALLS, NATIONAL_USAGE_CALLS, IMEI_DISTINCT_COUNT, CELL_DISTINCT_COUNT, \n \
	GPRS_USAGE_CLICKS, SMS_MO_USAGE_CLICKS, SMS_MT_USAGE_CLICKS, MTC_USAGE_CALLS, MIN_MOB_NET_COUNT \n \
	from detection_usg_tbl where AGGREGATION_ID = %d AND DETECTION_STATUS=0", a_cursor.aggregation_id[agg_cur]);

    do
    {
        rc = sqlite3_prepare(db, my_sql, -1, &pStmt, 0);
        if(rc != SQLITE_OK) {
            FW_log("e", "SQL error: %s", sqlite3_errmsg(db));
            FW_log("e", "SQL stat : %s", my_sql);
            sqlite3_free(err_msg);
            return 1;
        }

        while( SQLITE_ROW==sqlite3_step(pStmt) )
        {

	/* Prepare Detection Structures 2 */
	memset(&d_cursor, 0, sizeof(d_cursor));
	memset(&complex, 0, sizeof(complex));

	FW_log("i","	record %d", sqlite3_column_int(pStmt, 0));
	d_cursor.DETECTION_ID = sqlite3_column_int(pStmt, 0);
	
	/* loaded values */
	strncpy(d_cursor.IMSI, (const char *) sqlite3_column_text(pStmt, 1), sizeof(d_cursor.IMSI));
	d_cursor.INTRA_NET_USAGE_CALLS = (int) sqlite3_column_int(pStmt, 2);
	d_cursor.INTER_NET_USAGE_CALLS = (int) sqlite3_column_int(pStmt, 3);
	d_cursor.MOBILE_NET_USAGE_CALLS = (int) sqlite3_column_int(pStmt, 4);
	d_cursor.INTERNATIONAL_USAGE_CALLS = (int) sqlite3_column_int(pStmt, 5);
	d_cursor.NATIONAL_USAGE_CALLS = (int) sqlite3_column_int(pStmt, 6);
	d_cursor.IMEI_DISTINCT_COUNT = (int) sqlite3_column_int(pStmt, 7);
	d_cursor.CELL_DISTINCT_COUNT = (int) sqlite3_column_int(pStmt, 8);
	d_cursor.GPRS_USAGE_CLICKS = (int) sqlite3_column_int(pStmt, 9);
	d_cursor.SMS_MO_USAGE_CLICKS = (int) sqlite3_column_int(pStmt, 10);
	d_cursor.SMS_MT_USAGE_CLICKS = (int) sqlite3_column_int(pStmt, 11);
	d_cursor.MTC_USAGE_CALLS = (int) sqlite3_column_int(pStmt, 12);
	d_cursor.MIN_MOB_NET_COUNT = (int) sqlite3_column_int(pStmt, 13);
	
	/* calculated values  */
	d_cursor.WHITE_LIST_ENTRY = 0; //white_list lookup function has to be implemented. For this example whitelist=N
	d_cursor.DISTINCT_USAGE_CALLS = d_cursor.MOBILE_NET_USAGE_CALLS + 1; //distinct calls can be figured out either in aggregator or in detector. For this example it is mobile_net_usage_calls + 1
	d_cursor.MOC_TOTAL_CALLS = d_cursor.MOBILE_NET_USAGE_CALLS + d_cursor.INTERNATIONAL_USAGE_CALLS + d_cursor.NATIONAL_USAGE_CALLS;
	d_cursor.SMS_TOTAL_CLICKS = d_cursor.SMS_MO_USAGE_CLICKS + d_cursor.SMS_MT_USAGE_CLICKS; //supposed to be implemented in aggregator. For this example is SMS_MO+SMS_MT
	d_cursor.GPRS_TOTAL_CLICKS = d_cursor.GPRS_USAGE_CLICKS + 10; //suppose to be implemented in aggregator. For this example is GPRS+10

	for (pat_cur=1;pat_cur<=pat_new;pat_cur++) {
	FW_log("i", "	pattern %d \n", pat_cur);
	
	complex[0] = pat_cur; //this is because insert shall be taken to the function, so it will save complex arr data
	if (!cmp_P(1, p_cursor.REF_VALUE_1[pat_cur], p_cursor.OPERATOR_1[pat_cur], d_cursor.INTRA_NET_USAGE_CALLS, d_cursor.MOC_TOTAL_CALLS)) continue;
	if (!cmp_P(2, p_cursor.REF_VALUE_2[pat_cur], p_cursor.OPERATOR_2[pat_cur], d_cursor.INTER_NET_USAGE_CALLS, d_cursor.MOC_TOTAL_CALLS)) continue;
	if (!cmp_P(3, p_cursor.REF_VALUE_3[pat_cur], p_cursor.OPERATOR_3[pat_cur], d_cursor.MOBILE_NET_USAGE_CALLS, d_cursor.MOC_TOTAL_CALLS)) continue;
	if (!cmp_P(4, p_cursor.REF_VALUE_4[pat_cur], p_cursor.OPERATOR_4[pat_cur], d_cursor.INTERNATIONAL_USAGE_CALLS, d_cursor.MOC_TOTAL_CALLS)) continue;
	if (!cmp_P(5, p_cursor.REF_VALUE_5[pat_cur], p_cursor.OPERATOR_5[pat_cur], d_cursor.NATIONAL_USAGE_CALLS, d_cursor.MOC_TOTAL_CALLS)) continue;
	if (!cmp_P(6, p_cursor.REF_VALUE_6[pat_cur], p_cursor.OPERATOR_6[pat_cur], d_cursor.DISTINCT_USAGE_CALLS, d_cursor.MOC_TOTAL_CALLS)) continue;

	if (!cmp_N(7, p_cursor.REF_VALUE_7[pat_cur], p_cursor.OPERATOR_7[pat_cur], d_cursor.IMEI_DISTINCT_COUNT)) continue;
	if (!cmp_N(8, p_cursor.REF_VALUE_8[pat_cur], p_cursor.OPERATOR_8[pat_cur], d_cursor.CELL_DISTINCT_COUNT)) continue;

	if (!cmp_P(9, p_cursor.REF_VALUE_9[pat_cur], p_cursor.OPERATOR_9[pat_cur], d_cursor.GPRS_USAGE_CLICKS, d_cursor.GPRS_TOTAL_CLICKS)) continue;
	if (!cmp_P(10, p_cursor.REF_VALUE_10[pat_cur], p_cursor.OPERATOR_10[pat_cur], d_cursor.SMS_MO_USAGE_CLICKS, d_cursor.SMS_TOTAL_CLICKS)) continue;
	if (!cmp_P(11, p_cursor.REF_VALUE_11[pat_cur], p_cursor.OPERATOR_11[pat_cur], d_cursor.SMS_MT_USAGE_CLICKS, d_cursor.SMS_TOTAL_CLICKS)) continue;
	if (!cmp_P(12, p_cursor.REF_VALUE_12[pat_cur], p_cursor.OPERATOR_12[pat_cur], d_cursor.MTC_USAGE_CALLS, d_cursor.MOC_TOTAL_CALLS)) continue;
	
	if (!cmp_N(13, p_cursor.REF_VALUE_13[pat_cur], p_cursor.OPERATOR_13[pat_cur], d_cursor.MIN_MOB_NET_COUNT)) continue;
	if (!cmp_B(14, p_cursor.REF_VALUE_14[pat_cur], p_cursor.OPERATOR_14[pat_cur], d_cursor.WHITE_LIST_ENTRY)) continue;

	FW_log("i", "true - Inserting Data");
        memset(my_sql, 0, sizeof(my_sql));
	sprintf(my_sql, "INSERT INTO ALARMS_USG_TBL(PATTERN_ID,   MOBILE_NET_USAGE,  INTERNATIONAL_USAGE,  NATIONAL_USAGE,  CALL_DIVERSITY_SPREAD, IMEI_DISTINCT_COUNT, CELL_DISTINCT_COUNT, GPRS_USAGE,  SMS_MO_USAGE,  SMS_MT_USAGE,  MTC_USAGE,  MIN_MOB_NET_COUNT,  WHITE_LIST_ENTRY,  ALARM_STATUS, UPDATES_COUNT) VALUES (%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d)", complex[0], complex[3], complex[4], complex[5], complex[6], complex[7], complex[8], complex[9], complex[10], complex[11], complex[12], complex[13], complex[14], 2, 1);

        if((rc=FW_sqlite_exec(db, my_sql)) != 0)  {
        FW_log("e", "Error on Insert: %s", my_sql);
        return 1;
        		}

		}

	FW_log("i","Update Status to Processed = 2");
        memset(my_sql, 0, sizeof(my_sql));
        sprintf(my_sql, "UPDATE DETECTION_USG_TBL SET DETECTION_STATUS=2 WHERE AGGREGATION_ID = %d AND DETECTION_ID = %d", a_cursor.aggregation_id[agg_cur], d_cursor.DETECTION_ID);

        if((rc=FW_sqlite_exec(db, my_sql)) != 0)  {
        FW_log("e", "Error on Insert: %s", my_sql);
        return 1;
        }

        }

        rc = sqlite3_finalize(pStmt);

    } while( rc==SQLITE_SCHEMA );

	/* Detection Table Cycle End */

	}

FW_log("i","sleeping %d", SLEEP_SECONDS);
sleep(SLEEP_SECONDS);
}

return rc;
}

/* cmp function */
int cmp(int *a, int *operator_id, int *b) {
int m=1;
/* if parameter not defined then we treat it as true */
if (*operator_id == 0) return m;

switch (*operator_id) {
case 1:
m = *a==*b;
break;
case 2:
m = *a>=*b;
break;
case 3:
m = *a<=*b;
break;
case 4:
m = *a>*b;
break;
case 5:
m = *a<*b;
break;
}

return m;
}

/* percentage */
int cmp_P(int param, int a, int operator_id, int b, int s) {
int result;

/* rounding has not been used */
double fraction = (double) b / s;
int n = fraction * NORMALIZER;
complex[param] = n;

result = cmp(&a, &operator_id, &n);
FW_log("i", "   cmp_P parameter id %d value %d operator %d ref value %d called %d total %d result %d", param, a, operator_id, n, b, s, result);

return result;
}

/* number */
int cmp_N(int param, int a, int operator_id, int b) {
int result = cmp(&a, &operator_id, &b);
complex[param] = b;

FW_log("i", "	cmp_N parameter id %d value %d operator %d ref value %d result %d", param, a, operator_id, b, result);

return result;
}



/* logical */
int cmp_B(int param, int a, int operator_id, int b) {
int result = 1;
complex[param] = b;

FW_log("i", "   cmp_B parameter id %d value %d operator %d ref value %d result %d", param, a, operator_id, b, result);
return result;
}

