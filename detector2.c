/***
For now the following things suppose to be checked and filled
by aggregator table:
So basically aggregator upon ugregation should check
WHITE_LIST_ENTRY and fill either 1 or 0
then
suppose to check different numbers called and fill DISTINCT_USAGE_CALLS
also suppose to calc total number of calls, sms, gprs
and fill
MOC_TOTAL_CALLS, SMS_TOTAL_CALLS, GPRS_TOTAL_CALLS
***/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "FWsql.h"
#include "FWlog.h"

#define CONTROL_FILENAME "detector.IND"

#define SLEEP_SECONDS 10

#define ARRAY_SIZE 40000

#define BUFFER_SIZE 2048
#define COEFF 1000

	struct P_CURSOR {
	int	PATTERN_ID[ARRAY_SIZE];
	int	OPERATOR_1[ARRAY_SIZE];
	int	REF_VALUE_1[ARRAY_SIZE];
        int     OPERATOR_2[ARRAY_SIZE]; 
	int     REF_VALUE_2[ARRAY_SIZE];
        int     OPERATOR_3[ARRAY_SIZE];
        int     REF_VALUE_3[ARRAY_SIZE];
        int     OPERATOR_4[ARRAY_SIZE]; 
	int     REF_VALUE_4[ARRAY_SIZE];
        int     OPERATOR_5[ARRAY_SIZE];
        int     REF_VALUE_5[ARRAY_SIZE];
	int     OPERATOR_6[ARRAY_SIZE];
	int     REF_VALUE_6[ARRAY_SIZE];
	int     OPERATOR_7[ARRAY_SIZE];
	int     REF_VALUE_7[ARRAY_SIZE];
	int     OPERATOR_8[ARRAY_SIZE];
	int     REF_VALUE_8[ARRAY_SIZE];
	int     OPERATOR_9[ARRAY_SIZE];
	int     REF_VALUE_9[ARRAY_SIZE];
	int     OPERATOR_10[ARRAY_SIZE];
	int     REF_VALUE_10[ARRAY_SIZE];
	int     OPERATOR_11[ARRAY_SIZE];
	int     REF_VALUE_11[ARRAY_SIZE];
	int     OPERATOR_12[ARRAY_SIZE];
	int     REF_VALUE_12[ARRAY_SIZE];
	int     OPERATOR_13[ARRAY_SIZE];
	int     REF_VALUE_13[ARRAY_SIZE];
	int     OPERATOR_14[ARRAY_SIZE];
	int     REF_VALUE_14[ARRAY_SIZE];
	};

char param[15][30] = {"DETECTION_ID", "INTRA_NET_USAGE_CALLS", "INTER_NET_USAGE_CALLS", "MOBILE_NET_USAGE_CALLS", "INTERNATIONAL_USAGE_CALLS", "NATIONAL_USAGE_CALLS", "DISTINCT_USAGE_CALLS", "IMEI_DISTINCT_COUNT", "CELL_DISTINCT_COUNT", "GPRS_USAGE_CLICKS", "SMS_MO_USAGE_CLICKS", "SMS_MT_USAGE_CLICKS", "MTC_USAGE_CALLS", "MIN_MOB_NET_COUNT", "WHITE_LIST_ENTRY"};
char param2[3][30] = {"MOC_TOTAL_CALLS", "SMS_TOTAL_CLICKS", "GPRS_TOTAL_CLICKS"};


char *param_builder(int, int, int, char, int);

int main()  {

    int             rc = 0;
    char            db_name[] = "sim_gateway.db";
    char            my_sql[4096];
    char*           err_msg = 0;

    sqlite3*        db = 0;
    sqlite3_stmt*   pStmt = 0;

	/* My local definitions */

	int pat_cur, pat_new, pat_cnt;
	int dbg_cur;

	char ctrl_flname[13] = CONTROL_FILENAME;
	struct stat st_ctrl;

	char	*param_buffer = (char *) malloc(2048);
	
	struct P_CURSOR p_cursor;

	/* Create file or quit */
    if(stat(ctrl_flname, &st_ctrl)>=0)
     {
         FW_log("i", "Usage error:\nIt is runnig another  process");
	return 1;
     }
     else
     { 
          if(creat(ctrl_flname,S_IREAD|S_IWRITE)<0)
          {
          FW_log("i", "Cannot create control file");
	return 1;
          }
          FW_log("i", "Created control file");
	  FW_log("i", "Begin of process");
     	}
	
    /* Open our DB again  */

    FW_log("i", "Opening DB %s", db_name);
    if (FW_sqlite_db_open(&db, db_name))
    {
        FW_log("e", "Error opening DB: %s", db_name);
        return 1;
    }

	/* Prepare pattern structures */
	
	memset(&p_cursor, 0, sizeof(p_cursor));
    	memset(&my_sql, 0, sizeof(my_sql)); 
	
    /* Pattern Routine Begin */

	sprintf(my_sql, "SELECT p1.pattern_id, p2.parameter_id, c1.priority, \n \
	o2.operator_id,ref_integer,r2.ref_type_code \n \
	FROM pattern_usg_tbl p1,chain_usg_tbl c1,parameter_def_tbl p2, \n \
	condition_usg_tbl c2,operator_def_tbl o2,ref_value_usg_tbl r2 \n \
	WHERE c1.pattern_id=p1.pattern_id AND c2.chain_id=c1.chain_id \n \
	AND p2.parameter_id = c2.parameter_id AND o2.operator_id=c2.operator_id AND r2.condition_id=c2.condition_id \n \
	order by c1.pattern_id, c1.priority");

    do
    {
        rc = sqlite3_prepare(db, my_sql, -1, &pStmt, 0);
        if(rc != SQLITE_OK) {
            FW_log("e", "SQL error: %s", sqlite3_errmsg(db));
            FW_log("e", "SQL stat : %s", my_sql);
            sqlite3_free(err_msg);
            return 1;
        }

        while( SQLITE_ROW==sqlite3_step(pStmt) )
        {
	pat_cnt = (int) sqlite3_column_int(pStmt, 0);
	p_cursor.PATTERN_ID[pat_cnt] = (int) sqlite3_column_int(pStmt, 0);

	switch ((int) sqlite3_column_int(pStmt, 1)) {
	case 1:	//INTRA_NET_USAGE
	p_cursor.OPERATOR_1[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);
	p_cursor.REF_VALUE_1[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
	break;
	case 2:	//INTER_NET_USAGE
	p_cursor.OPERATOR_2[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);
	p_cursor.REF_VALUE_2[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
	break;
	case 3:	//MOBILE_NET_USAGE
        p_cursor.OPERATOR_3[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);
        p_cursor.REF_VALUE_3[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
        break;
	case 4:	//INTERNATIONAL_USAGE
        p_cursor.OPERATOR_4[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);
        p_cursor.REF_VALUE_4[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
        break;
	case 5:	//NATIONAL_USAGE
        p_cursor.OPERATOR_5[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);
        p_cursor.REF_VALUE_5[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
        break;
	case 6:	//CALL_DIVERSITY_SPREAD
	p_cursor.OPERATOR_6[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);
	p_cursor.REF_VALUE_6[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
	break;
	case 7: //IMEI_DISTINCT_COUNT     
	p_cursor.OPERATOR_7[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);       
	p_cursor.REF_VALUE_7[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
	break;
	case 8: //CELL_DISTINCT_COUNT     
	p_cursor.OPERATOR_8[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);       
	p_cursor.REF_VALUE_8[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
	break;
	case 9: //GPRS_USAGE     
	p_cursor.OPERATOR_9[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);       
	p_cursor.REF_VALUE_9[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
	break;
	case 10: //SMS_MO_USAGE     
	p_cursor.OPERATOR_10[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);       
	p_cursor.REF_VALUE_10[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
	break;
	case 11: //SMS_MT_USAGE     
	p_cursor.OPERATOR_11[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);       
	p_cursor.REF_VALUE_11[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
	break;
	case 12: //MTC_USAGE    
	p_cursor.OPERATOR_12[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);       
	p_cursor.REF_VALUE_12[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
	break;
	case 13: //MIN_MOB_NET_COUNT
	p_cursor.OPERATOR_13[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);       
	p_cursor.REF_VALUE_13[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
	break;
	case 14: //WHITE_LIST_ENTRY
	p_cursor.OPERATOR_14[pat_cnt] = (int) sqlite3_column_int(pStmt, 3);       
	p_cursor.REF_VALUE_14[pat_cnt] = (int) sqlite3_column_int(pStmt, 4);
	break;
	}

        }

	pat_new = pat_cnt;
        rc = sqlite3_finalize(pStmt);

    } while( rc==SQLITE_SCHEMA );

	/* Pattern Routine End */
	
    	/* Print loaded pattern buffers */
	for(dbg_cur = 1;dbg_cur<=pat_new;dbg_cur++) {
	FW_log("i"," pattern %d | %d %d | %d %d | %d %d | %d %d | %d %d | %d %d | %d %d | %d %d | %d %d | %d %d | %d %d | %d %d | %d %d | %d %d ", 
		p_cursor.PATTERN_ID[dbg_cur], 
		p_cursor.OPERATOR_1[dbg_cur], p_cursor.REF_VALUE_1[dbg_cur], 
		p_cursor.OPERATOR_2[dbg_cur], p_cursor.REF_VALUE_2[dbg_cur], 
		p_cursor.OPERATOR_3[dbg_cur], p_cursor.REF_VALUE_3[dbg_cur], 
		p_cursor.OPERATOR_4[dbg_cur], p_cursor.REF_VALUE_4[dbg_cur], 
		p_cursor.OPERATOR_5[dbg_cur], p_cursor.REF_VALUE_5[dbg_cur], 
		p_cursor.OPERATOR_6[dbg_cur], p_cursor.REF_VALUE_6[dbg_cur], 
		p_cursor.OPERATOR_7[dbg_cur], p_cursor.REF_VALUE_7[dbg_cur],
		p_cursor.OPERATOR_8[dbg_cur], p_cursor.REF_VALUE_8[dbg_cur],
		p_cursor.OPERATOR_9[dbg_cur], p_cursor.REF_VALUE_9[dbg_cur],
		p_cursor.OPERATOR_10[dbg_cur], p_cursor.REF_VALUE_10[dbg_cur],
		p_cursor.OPERATOR_11[dbg_cur], p_cursor.REF_VALUE_11[dbg_cur],
		p_cursor.OPERATOR_12[dbg_cur], p_cursor.REF_VALUE_12[dbg_cur],
		p_cursor.OPERATOR_13[dbg_cur], p_cursor.REF_VALUE_13[dbg_cur],
		p_cursor.OPERATOR_14[dbg_cur], p_cursor.REF_VALUE_14[dbg_cur]
		);
	}


	/* Start of Main Cycle */
	while(1>0)
	{
	FW_log("i","Cycle starts");

	/* Checking Control File for existence */
     if(stat(ctrl_flname,&st_ctrl)<0)
     {
        FW_log("i","Control File Not Found, quiting");
	FW_log("i", "Close DB");
	if(sqlite3_close(db))
	{
        FW_log("e", "SQL error closing Work DB: %s", sqlite3_errmsg(db));
        return 1;
	}

	FW_log("i", "End of process");
	FW_resetProg();
        return rc;
     }

     for (pat_cur = 1;pat_cur<=pat_new;pat_cur++) {
	FW_log("i", "	pattern %d \n", pat_cur);
	 
	/* Detection Table Cycle Begin */

	/* Prepare Detection Structures 1 */
        memset(&my_sql, 0, sizeof(my_sql));

	sprintf(my_sql,"select DETECTION_ID, IMSI, \n \
	INTRA_NET_USAGE_CALLS, \n \
	INTER_NET_USAGE_CALLS, \n \
	(MOBILE_NET_USAGE_CALLS*1000/MOC_TOTAL_CALLS*1000) MOBILE_NET_USAGE_CALLS, \n \
	(INTERNATIONAL_USAGE_CALLS*1000/MOC_TOTAL_CALLS*1000) INTERNATIONAL_USAGE_CALLS, \n \
	(NATIONAL_USAGE_CALLS*1000/MOC_TOTAL_CALLS*1000) NATIONAL_USAGE_CALLS, \n \
	(DISTINCT_USAGE_CALLS*1000/MOC_TOTAL_CALLS*1000) CALL_DIVERSITY_SPREAD, \n \
	IMEI_DISTINCT_COUNT,  \n \
	CELL_DISTINCT_COUNT, \n \
	(GPRS_USAGE_CLICKS*1000/GPRS_TOTAL_CLICKS*1000) GPRS_USAGE_CLICKS, \n \
	(SMS_MO_USAGE_CLICKS*1000/SMS_TOTAL_CLICKS*1000) SMS_MO_USAGE_CLICKS, \n \
	(SMS_MT_USAGE_CLICKS*1000/SMS_TOTAL_CLICKS*1000) SMS_MT_USAGE_CLICKS, \n \
	(MTC_USAGE_CALLS*1000/MOC_TOTAL_CALLS*1000) MTC_USAGE_CALLS, \n \
	MIN_MOB_NET_COUNT, \n \
	WHITE_LIST_ENTRY  \n \
	from detection_usg_tbl where AGGREGATION_ID = 15 AND DETECTION_STATUS=0");

	/* Begin */

	memset(param_buffer, 0, sizeof(param_buffer));

	if (p_cursor.OPERATOR_1[pat_cur]) { //INTRA_NET_USAGE
	param_buffer = param_builder(1, p_cursor.OPERATOR_1[pat_cur], p_cursor.REF_VALUE_1[pat_cur], 'P', 0);
	strncat(my_sql, param_buffer, strlen(param_buffer));
	}
	if (p_cursor.OPERATOR_2[pat_cur]) { //INTER_NET_USAGE
        param_buffer = param_builder(2, p_cursor.OPERATOR_2[pat_cur], p_cursor.REF_VALUE_2[pat_cur], 'P', 0);
	strncat(my_sql, param_buffer, strlen(param_buffer));
	}
        if (p_cursor.OPERATOR_3[pat_cur]) { //MOBILE_NET_USAGE
        param_buffer = param_builder(3, p_cursor.OPERATOR_3[pat_cur], p_cursor.REF_VALUE_3[pat_cur], 'P', 0);
        strncat(my_sql, param_buffer, strlen(param_buffer));
        }
        if (p_cursor.OPERATOR_4[pat_cur]) { //INTERNATIONAL_USAGE
        param_buffer = param_builder(4, p_cursor.OPERATOR_4[pat_cur], p_cursor.REF_VALUE_4[pat_cur], 'P', 0);
        strncat(my_sql, param_buffer, strlen(param_buffer));
        }
        if (p_cursor.OPERATOR_5[pat_cur]) { //NATIONAL_USAGE
        param_buffer = param_builder(5, p_cursor.OPERATOR_5[pat_cur], p_cursor.REF_VALUE_5[pat_cur], 'P', 0);
        strncat(my_sql, param_buffer, strlen(param_buffer));
        }
        if (p_cursor.OPERATOR_6[pat_cur]) { //CALL_DIVERSITY_SPREAD
        param_buffer = param_builder(6, p_cursor.OPERATOR_6[pat_cur], p_cursor.REF_VALUE_6[pat_cur], 'P', 0);
        strncat(my_sql, param_buffer, strlen(param_buffer));
        }
	if (p_cursor.OPERATOR_7[pat_cur]) { //IMEI_DISTINCT_COUNT
	param_buffer = param_builder(7, p_cursor.OPERATOR_7[pat_cur], p_cursor.REF_VALUE_7[pat_cur], 'N', 0); 
	strncat(my_sql, param_buffer, strlen(param_buffer)); 
	}
	if (p_cursor.OPERATOR_8[pat_cur]) { //CELL_DISTINCT_COUNT
	param_buffer = param_builder(8, p_cursor.OPERATOR_8[pat_cur], p_cursor.REF_VALUE_8[pat_cur], 'N', 0); 
	strncat(my_sql, param_buffer, strlen(param_buffer)); 
	}
        if (p_cursor.OPERATOR_9[pat_cur]) { //GPRS_USAGE
        param_buffer = param_builder(9, p_cursor.OPERATOR_9[pat_cur], p_cursor.REF_VALUE_9[pat_cur], 'P', 1);
        strncat(my_sql, param_buffer, strlen(param_buffer));
        }
        if (p_cursor.OPERATOR_10[pat_cur]) { //SMS_MO_USAGE
        param_buffer = param_builder(10, p_cursor.OPERATOR_10[pat_cur], p_cursor.REF_VALUE_10[pat_cur], 'P', 1);
        strncat(my_sql, param_buffer, strlen(param_buffer));
        }
	if (p_cursor.OPERATOR_11[pat_cur]) { //SMS_MT_USAGE
        param_buffer = param_builder(11, p_cursor.OPERATOR_11[pat_cur], p_cursor.REF_VALUE_11[pat_cur], 'P', 1);
        strncat(my_sql, param_buffer, strlen(param_buffer));
        }
        if (p_cursor.OPERATOR_12[pat_cur]) { //MTC_USAGE
        param_buffer = param_builder(12, p_cursor.OPERATOR_12[pat_cur], p_cursor.REF_VALUE_12[pat_cur], 'P', 0);
        strncat(my_sql, param_buffer, strlen(param_buffer));
        }
        if (p_cursor.OPERATOR_13[pat_cur]) { //MIN_MOB_NET_COUNT
        param_buffer = param_builder(13, p_cursor.OPERATOR_13[pat_cur], p_cursor.REF_VALUE_13[pat_cur], 'N', 0);
        strncat(my_sql, param_buffer, strlen(param_buffer));
        }
        if (p_cursor.OPERATOR_14[pat_cur]) { //WHITE_LIST_ENTRY
        param_buffer = param_builder(14, p_cursor.OPERATOR_14[pat_cur], p_cursor.REF_VALUE_14[pat_cur], 'B', 0);
        strncat(my_sql, param_buffer, strlen(param_buffer));
        }

	FW_log("i","%s",my_sql);	

	free(param_buffer);
	/* End */	
	
    do
    {
        rc = sqlite3_prepare(db, my_sql, -1, &pStmt, 0);
        if(rc != SQLITE_OK) {
            FW_log("e", "SQL error: %s", sqlite3_errmsg(db));
            FW_log("e", "SQL stat : %s", my_sql);
            sqlite3_free(err_msg);
            return 1;
        }

        while( SQLITE_ROW==sqlite3_step(pStmt) )
        {

	/* Prepare Detection Structures 2 */
	FW_log("i","	detection_id %d", sqlite3_column_int(pStmt, 0));
	
	FW_log("i", "true - Inserting Data");
        memset(my_sql, 0, sizeof(my_sql));
	sprintf(my_sql, "INSERT INTO ALARMS_USG_TBL(PATTERN_ID, DETECTION_ID, MOBILE_NET_USAGE, INTERNATIONAL_USAGE,  NATIONAL_USAGE, CALL_DIVERSITY_SPREAD, IMEI_DISTINCT_COUNT, CELL_DISTINCT_COUNT, GPRS_USAGE, SMS_MO_USAGE, SMS_MT_USAGE, MTC_USAGE, MIN_MOB_NET_COUNT, WHITE_LIST_ENTRY, ALARM_STATUS, UPDATES_COUNT, CREATE_DATETIME) VALUES (%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %s)", pat_cur, sqlite3_column_int(pStmt, 0), sqlite3_column_int(pStmt, 4), sqlite3_column_int(pStmt, 5), sqlite3_column_int(pStmt, 6), sqlite3_column_int(pStmt, 7), sqlite3_column_int(pStmt, 8), sqlite3_column_int(pStmt, 9), sqlite3_column_int(pStmt, 10), sqlite3_column_int(pStmt, 11), sqlite3_column_int(pStmt, 12), sqlite3_column_int(pStmt, 13), sqlite3_column_int(pStmt, 14), sqlite3_column_int(pStmt, 15), 2, 1, "DATETIME('NOW','+1 HOUR')");

	FW_log("i","%s",my_sql);
	
        if((rc=FW_sqlite_exec(db, my_sql)) != 0)  {
        FW_log("e", "Error on Insert: %s", my_sql);
        return 1;
        }

        }
        rc = sqlite3_finalize(pStmt);

    } while( rc==SQLITE_SCHEMA );

	/* Detection Table Cycle End */
}

	
	/* Log Begin */
	
	FW_log("i","Log To Control Table");
        memset(my_sql, 0, sizeof(my_sql));
	sprintf(my_sql, "INSERT INTO CONTROL_USG_TBL (LAST_CHECK) VALUES (DATETIME('NOW','+1 HOUR'))");
	
        if((rc=FW_sqlite_exec(db, my_sql)) != 0)  {
        FW_log("e", "Error on Insert: %s", my_sql);
        return 1;
        }
	
	/* Log End */
	
FW_log("i","sleeping %d", SLEEP_SECONDS);
sleep(SLEEP_SECONDS);
}

return rc;
}

char *param_builder(int param_id, int operator_id, int ref_value, char type_code, int  denominator_id) {

char *buffer = (char *) malloc(BUFFER_SIZE);
char *min = (char *) malloc(BUFFER_SIZE);

switch (type_code) {
case 'P':
sprintf(buffer," %s (%s*%d/%s*%d) ", "AND", param[param_id], COEFF, param2[denominator_id], COEFF);
break;
case 'N':
sprintf(buffer," %s %s ", "AND", param[param_id]);
break;
case 'B':
sprintf(buffer," %s %s ", "AND", param[param_id]);
break;
}


switch (operator_id) {
case 1:
sprintf(min, " %s %d ", "=", ref_value);
break;
case 2:
sprintf(min, " %s %d ", ">=", ref_value);
break;
case 3:
sprintf(min, " %s %d ", "<=", ref_value);
break;
case 4:
sprintf(min, " %c %d ", '>', ref_value);
break;
case 5:
sprintf(min, " %c %d ", '<', ref_value);
break;
}

strncat(buffer, min, strlen(min));

free(min);
return buffer;
}


